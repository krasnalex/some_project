<?php
?>
<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Бронирование</title>
    <link rel="icon" href="/img/house.ico">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/js/bootstrap.min.js">
</head>

<body class="w-50 m-auto vh-100">
    <h1 class="w-50 m-auto mt-5 mb-5">Заявка на бронирование</h1>
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="/bookings/submit" class="container h-100 mt-3" method="post">
        @csrf
        <div class="form-group mb-3">
            <label class="mb-2">Ваше ФИО</label>
            <input type="text" class="form-control" name="name" placeholder="Введите ФИО">
        </div>
        <div class="form-group mb-3">
            <label class="mb-2">Имя питомца</label>
            <input type="text" class="form-control" name="name-pet" placeholder="Введите имя питомца">
        </div>
        <div class="form-group mb-3">
            <label class="mb-2">Номер телефона</label>
            <input type="text" class="form-control" name="phone" placeholder="Введите номер телефона">
        </div>
        <div class="form-group mb-3">
            <label class="mb-2">Ваш e-mail</label>
            <input type="email" class="form-control" name="email" placeholder="Введите email">
        </div>
        <div class="form-group w-25 mb-3">
            <label class="mb-2">Дата заезда</label>
            <input type="date" class="form-control" name="date-start">
        </div>
        <div class="form-group w-25 mb-3">
            <label class="mb-2">Дата выезда</label>
            <input type="date" class="form-control" name="date-end">
        </div>
        <button type="submit" class="btn btn-success mt-3">Отправить заявку</button>
    </form>
</body>
</html>
