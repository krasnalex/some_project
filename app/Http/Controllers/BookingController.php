<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    public function create()
    {
        return view('forms.create.booking');
    }

    public function store(BookingRequest $request)
    {
        $validated = $request->validated();

        DB::table('bookings')->insert([
            'name_client' =>  $validated['name'],
            'name_pet' => $validated['name-pet'],
            'phone_client' => $validated['phone'],
            'email_client' => $validated['email'],
            'date_arrival' => $validated['date-start'],
            'date_departure' => $validated['date-end'],
        ]);
        return view("success");
    }
}
