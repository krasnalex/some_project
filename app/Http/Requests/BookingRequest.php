<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5',
            'name-pet' => 'required',
            'phone' => 'required|min:11|max:11',
            'email' => 'required|email',
            'date-start' => 'required|date',
            'date-end' => 'required|date',
        ];
    }

    public function attributes()
    {
         return [
             'name' => 'Имя',
             'name-pet' => 'Имя питомца',
             'phone' => 'Номер телефона',
             'email' => 'Email address',
             'date-start' => 'Дата заезда',
             'date-end' => 'Дата выезда',
         ];
    }

    public function messages()
    {
       return[
           'name.required' => 'Поле Имя является обязательным',
           'name-pet.required' => 'Поле Имя питомца является обязательным',
           'phone.required' => 'Поле Номер телефона является обязательным',
           'email.required' => 'Поле Email является обязательным',
           'date-start.required' => 'Поле Дата заезда является обязательным',
           'date-end.required' => 'Поле Дата выезда является обязательным',
           'amount.required' => 'Поле Количество персон является обязательным',

           'name.min' => 'Поле ФИО должно быть как минимум 5 символов',
           'phone.min' => 'Некоректно введенный номер телефона',
           'phone.max' => 'Некоректно введенный номер телефона',
           'email.email' => 'Некорректно введенный email',
           'date-start.data' => 'Некоректная Дата заезда',
           'date-end.data' => 'Некоректная Дата выезда',
           'amount.integer' => 'Поле Количество персон должно быть числом'
       ];
    }
}
