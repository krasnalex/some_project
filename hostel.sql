-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 01 2023 г., 11:59
-- Версия сервера: 8.0.19
-- Версия PHP: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `hostel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `addresses`
--

CREATE TABLE `addresses` (
  `id_address` int NOT NULL,
  `id_city` int NOT NULL,
  `text_address` varchar(255) NOT NULL,
  `phone_number` varchar(11) NOT NULL,
  `timetable` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `addresses`
--

INSERT INTO `addresses` (`id_address`, `id_city`, `text_address`, `phone_number`, `timetable`) VALUES
(1, 1, 'ул. Удмуртская, 646', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `admins`
--

CREATE TABLE `admins` (
  `id_admin` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `admins`
--

INSERT INTO `admins` (`id_admin`, `name`, `email`, `phone`) VALUES
(1, 'Согазонов Владислав Андреевич', 'sogazvl@example.com', '89118798656');

-- --------------------------------------------------------

--
-- Структура таблицы `bookings`
--

CREATE TABLE `bookings` (
  `id_booking` int NOT NULL,
  `id_room` int NOT NULL DEFAULT '1',
  `id_admin` int NOT NULL DEFAULT '1',
  `name_client` varchar(255) NOT NULL,
  `name_pet` varchar(255) NOT NULL,
  `phone_client` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email_client` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `date_arrival` date NOT NULL,
  `date_departure` date NOT NULL,
  `status_accepted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `bookings`
--

INSERT INTO `bookings` (`id_booking`, `id_room`, `id_admin`, `name_client`, `name_pet`, `phone_client`, `email_client`, `date_arrival`, `date_departure`, `status_accepted`) VALUES
(1, 1, 1, 'Владов Владислав Владович', 'Котан', '89003347784', 'kotano@example.com', '2023-04-06', '2023-04-07', 0),
(2, 2, 1, 'Пиков Ян Яковлевич', 'Рыжий', '89248909009', 'ryzsh@example.com', '2023-04-12', '2023-04-14', 0),
(3, 3, 1, 'Угаков Иван Дмитриевич', 'Геннадий', '89564356575', 'ivadsa@example.com', '2023-04-25', '2023-04-27', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE `cities` (
  `id_city` int NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id_city`, `name`) VALUES
(1, 'Ижевск'),
(2, 'Москва'),
(3, 'Самара');

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `id_contact` int NOT NULL,
  `id_address` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` char(11) NOT NULL,
  `timetable` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`id_contact`, `id_address`, `email`, `phone`, `timetable`) VALUES
(1, 1, 'kotek@example.com', '8945643421', 'Каждый день 8:00 - 20:00');

-- --------------------------------------------------------

--
-- Структура таблицы `info`
--

CREATE TABLE `info` (
  `id_info` int NOT NULL,
  `id_city` int NOT NULL,
  `slogan` varchar(50) NOT NULL,
  `name_website` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `info`
--

INSERT INTO `info` (`id_info`, `id_city`, `slogan`, `name_website`) VALUES
(1, 1, 'Котейки вперед!', 'Котейка18');

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id_review` int NOT NULL,
  `id_client` int NOT NULL,
  `id_address` int NOT NULL,
  `text_review` text NOT NULL,
  `date_review` varchar(50) NOT NULL,
  `name_client` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id_review`, `id_client`, `id_address`, `text_review`, `date_review`, `name_client`) VALUES
(1, 1, 1, 'Очень хорошее заведение! моей кошечке понравилось!', '05-06-2023', 'Степанов Иван Иванович'),
(2, 1, 1, 'Хорошее', '03-06-2023', 'Некашева Анна Васильевна'),
(3, 1, 1, 'Всегда мечтала о таком месте, помогает заняться своими делами, пока мои кошечки отдыхают тут', '07-04-2023', 'Гуляева Айгуль Маратовна'),
(4, 1, 1, 'Очень хорошее', '22-03-2023', 'Гитова Яна Васильевна'),
(5, 1, 1, 'Очень уютно', '11-09-2022', 'Васева Дарья Олеговна'),
(6, 1, 1, 'Нам понравилось, хорошее', '05-08-2023', 'Талова Любовь Маратовна');

-- --------------------------------------------------------

--
-- Структура таблицы `riggings`
--

CREATE TABLE `riggings` (
  `id_rigging` int NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `riggings`
--

INSERT INTO `riggings` (`id_rigging`, `name`) VALUES
(1, 'Лежак'),
(2, 'Домик'),
(3, 'Игровой комплекс - 3 яруса'),
(4, 'Когтеточка');

-- --------------------------------------------------------

--
-- Структура таблицы `riggings_n_rooms`
--

CREATE TABLE `riggings_n_rooms` (
  `id_riggings_n_rooms` int NOT NULL,
  `id_room` int NOT NULL,
  `id_rigging` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `riggings_n_rooms`
--

INSERT INTO `riggings_n_rooms` (`id_riggings_n_rooms`, `id_room`, `id_rigging`) VALUES
(1, 2, 3),
(2, 2, 2),
(3, 1, 1),
(4, 3, 1),
(5, 3, 2),
(6, 3, 3),
(7, 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `rooms`
--

CREATE TABLE `rooms` (
  `id_room` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `square` float NOT NULL,
  `price_per_day` int NOT NULL,
  `img_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `size` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `rooms`
--

INSERT INTO `rooms` (`id_room`, `name`, `square`, `price_per_day`, `img_name`, `size`) VALUES
(1, 'Эконом', 0.63, 260, 'room_economy@1x', '100х120х170'),
(2, 'Комфорт', 1.13, 700, 'room-comfort@1x', '100х125х180'),
(3, 'Сьют', 1.56, 350, 'room-suite@1x', '125х125х180'),
(4, 'Эконом плюс', 0.9, 270, 'room-economy-plus@1x', '90х100х180'),
(5, 'Люкс', 2.56, 500, 'room-lux@1x', '140х120х190'),
(6, 'Супер-люкс', 2.88, 900, 'room-lux-super@1x', '200х200х200');

-- --------------------------------------------------------

--
-- Структура таблицы `social_networks`
--

CREATE TABLE `social_networks` (
  `id_social_network` int NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `social_networks`
--

INSERT INTO `social_networks` (`id_social_network`, `link`) VALUES
(1, 'https://vk.com/'),
(2, 'https://web.telegram.org/');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id_address`),
  ADD KEY `addresses_fk0` (`id_city`);

--
-- Индексы таблицы `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id_admin`);

--
-- Индексы таблицы `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id_booking`),
  ADD KEY `bookings_fk0` (`id_room`),
  ADD KEY `bookings_fk1` (`id_admin`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id_city`);

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id_contact`),
  ADD KEY `contacts_fk0` (`id_address`);

--
-- Индексы таблицы `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id_info`),
  ADD KEY `info_fk0` (`id_city`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id_review`),
  ADD KEY `reviews_fk0` (`id_client`),
  ADD KEY `reviews_fk1` (`id_address`);

--
-- Индексы таблицы `riggings`
--
ALTER TABLE `riggings`
  ADD PRIMARY KEY (`id_rigging`);

--
-- Индексы таблицы `riggings_n_rooms`
--
ALTER TABLE `riggings_n_rooms`
  ADD PRIMARY KEY (`id_riggings_n_rooms`),
  ADD KEY `riggings_n_rooms_fk0` (`id_room`),
  ADD KEY `riggings_n_rooms_fk1` (`id_rigging`);

--
-- Индексы таблицы `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id_room`);

--
-- Индексы таблицы `social_networks`
--
ALTER TABLE `social_networks`
  ADD PRIMARY KEY (`id_social_network`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id_address` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `admins`
--
ALTER TABLE `admins`
  MODIFY `id_admin` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id_booking` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id_city` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id_contact` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `info`
--
ALTER TABLE `info`
  MODIFY `id_info` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id_review` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `riggings`
--
ALTER TABLE `riggings`
  MODIFY `id_rigging` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `riggings_n_rooms`
--
ALTER TABLE `riggings_n_rooms`
  MODIFY `id_riggings_n_rooms` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id_room` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `social_networks`
--
ALTER TABLE `social_networks`
  MODIFY `id_social_network` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_fk0` FOREIGN KEY (`id_city`) REFERENCES `cities` (`id_city`);

--
-- Ограничения внешнего ключа таблицы `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_fk0` FOREIGN KEY (`id_room`) REFERENCES `rooms` (`id_room`),
  ADD CONSTRAINT `bookings_fk1` FOREIGN KEY (`id_admin`) REFERENCES `admins` (`id_admin`);

--
-- Ограничения внешнего ключа таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_fk0` FOREIGN KEY (`id_address`) REFERENCES `addresses` (`id_address`);

--
-- Ограничения внешнего ключа таблицы `info`
--
ALTER TABLE `info`
  ADD CONSTRAINT `info_fk0` FOREIGN KEY (`id_city`) REFERENCES `cities` (`id_city`);

--
-- Ограничения внешнего ключа таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_fk0` FOREIGN KEY (`id_client`) REFERENCES `admins` (`id_admin`),
  ADD CONSTRAINT `reviews_fk1` FOREIGN KEY (`id_address`) REFERENCES `addresses` (`id_address`);

--
-- Ограничения внешнего ключа таблицы `riggings_n_rooms`
--
ALTER TABLE `riggings_n_rooms`
  ADD CONSTRAINT `riggings_n_rooms_fk0` FOREIGN KEY (`id_room`) REFERENCES `rooms` (`id_room`),
  ADD CONSTRAINT `riggings_n_rooms_fk1` FOREIGN KEY (`id_rigging`) REFERENCES `riggings` (`id_rigging`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
